package ee.ut.math.tvt.salessystem.ui;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.HibernateSalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.Category;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.SoldPurchase;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.History;
import ee.ut.math.tvt.salessystem.logic.QuantityException;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import ee.ut.math.tvt.salessystem.logic.Warehouse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import java.io.*;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Properties;

/**
 * A simple CLI (limited functionality).
 */
public class ConsoleUI {
    private static final Logger log = LogManager.getLogger(ConsoleUI.class);

    //private final HibernateSalesSystemDAO dao;
    private final InMemorySalesSystemDAO dao;
    private final ShoppingCart cart;
    private final Warehouse warehouse;
    private final History history;

    public ConsoleUI() {
        //dao = new HibernateSalesSystemDAO();
        dao = new InMemorySalesSystemDAO();
        cart = new ShoppingCart(dao);
        warehouse = new Warehouse(dao);
        history = new History(dao, warehouse);
    }

    public static void main(String[] args) throws Exception {
        ConsoleUI console = new ConsoleUI();
        console.run();
    }

    /**
     * Run the sales system CLI.
     */
    private static void echo(Object o) {
        System.out.println(o);
    }

    public void run() throws IOException {
        System.out.println("===========================");
        System.out.println("=       Sales System      =");
        System.out.println("===========================");

        List<StockItem> stockItems = warehouse.getAllStockItems();
        List<SoldItem> soldItems = history.getAllSoldItems();
        List<Category> categories = dao.getCategories();
        List<SoldPurchase> soldPurchases = history.getAllSoldPurchases();

        echo("\n\n\n\n" +
                " ==================================================================== \n" +
                " =====                                                          ===== \n" +
                " =====                   DATABASE CONTENTS                      ===== \n" +
                " =====                                                          ===== \n" +
                " ==================================================================== \n"
        );


        echo("\n\n\n\n" +
                " ********************************************** \n" +
                " **              STOCKITEM                   ** \n" +
                " ********************************************** \n" +
                "\n"
        );

        echo("Found " + stockItems.size() + " stockitems:");
        for (StockItem s : stockItems) {
            echo(s);
        }


        echo("\n\n\n\n" +
                " ********************************************** \n" +
                " **              SOLDITEM                    ** \n" +
                " ********************************************** \n" +
                "\n"
        );

        echo("Found " + soldItems.size() + " solditems:");
        for (SoldItem s : soldItems) {
            echo(s);
        }


        echo("\n\n\n\n" +
                " ********************************************** \n" +
                " **             CATEGORY                     ** \n" +
                " ********************************************** \n" +
                "\n"
        );

        echo("Found " + categories.size() + " categories:");
        for (Category c : categories) {
            echo(c);
        }


        echo("\n\n\n\n" +
                " ********************************************** \n" +
                " **              SOLDPURCHASE                ** \n" +
                " ********************************************** \n" +
                "\n"
        );

        echo("Found " + soldPurchases.size() + " soldpurchases:");
        for (SoldPurchase s : soldPurchases) {
            echo(s);
        }
        echo("\n\n\n\n" +
                " ==================================================================== \n" +
                " =====                                                          ===== \n" +
                " =====             END OF DATABASE CONTENT                      ===== \n" +
                " =====                                                          ===== \n" +
                " ==================================================================== \n"
        );
        printUsage();
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            System.out.print("> ");
            processCommand(in.readLine().trim());
            System.out.println("Done. ");
        }
    }

    /**
     * Method for printing StockItem list
     **/
    private void printStockItemList(List<StockItem> stockItemList, String emptyListMessage) {
        System.out.println("-------------------------");
        for (StockItem si : stockItemList) {
            System.out.println(si.getId() + " " + si.getName() + " " + si.getPrice() + " eur (" + si.getQuantity() + " items in stock, recommended stock: " + si.getLowQuantity() + ")");
        }
        if (stockItemList.isEmpty())
            System.out.println(emptyListMessage);
        System.out.println("-------------------------");
    }

    /**
     * Method for printing SoldPurchase list
     **/
    private void printSoldPurchaseList(List<SoldPurchase> soldPurchaseList){
        for (SoldPurchase sp : soldPurchaseList) {
            System.out.println("---");
            System.out.println(sp);
            for (SoldItem j : history.getAllSoldItemsOfSelectedSoldPurchase(sp))
                System.out.println(j.toString());
            System.out.println("---");
        }
    }

    /**
     * Method for showing the warehouse content
     **/
    private void showStock() {
        log.info("Show stock items");
        printStockItemList(warehouse.getAllStockItems(), "No stock items found");
    }

    /**
     * Method for searching StockItem by ID
     **/
    private void findStock(String id) {
        log.info("Searching for an item with the barcode " + id);
        try {
            printStockItemList(warehouse.searchStockItemByBarcode(id), "No stock items found");
        } catch (NumberFormatException e) {
            log.error(e.getMessage());
            System.out.println(e.getMessage());
        }
    }

    /**
     * Method for searching for StockItem by name
     **/
    private void findStockName(String name) {
        log.info("Searching for an item with the name: " + name);
        List<StockItem> stockItems = warehouse.searchStockItemByName(name);
        printStockItemList(stockItems, "No stock items found");
    }

    /**
     * Method for confirming the purchase
     **/
    private void purchaseCart() {
        log.debug("Contents of the current basket:\n" + cart.getAllSoldItemsInShoppingCart());
        cart.submitCurrentPurchase();
    }

    /**
     * Method for adding an item to the cart
     **/
    private void addToCart(String barcode, String quantity) {
        try {
            cart.addSoldItem(barcode, quantity, true);
        } catch (Exception e) {
            if (e.getMessage().equals("Stock quantity exceeded")) {
                System.out.println("Inserted quantity exceeded stock quantity. The current available quantity is "
                        + cart.getStockItemQuantity(barcode));
            } else
                System.out.println("Illegal input");
        }
    }

    /**
     * Method for removing item from the shopping cart or reducing quantity of item
     **/
    private void removeFromCart(String barcode, String quantity){
        try{
            cart.removeSoldItem(barcode, quantity);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Method for creating a new StockItem
     **/
    private void addNewStockItem(String id, String name, String price, String quantity){
        try {
            Boolean add = false;
            if (warehouse.getStockItemByBarcode(Long.parseLong(id)) != null)
                add = true;
            warehouse.addItem(Long.parseLong(id), name, Double.parseDouble(quantity), Double.parseDouble(price), add);
        } catch (SalesSystemException | NoSuchElementException e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * Method for adjusting StockItem price
     **/
    private void adjustPrice(String id, String newPrice){
        try {
            StockItem item = warehouse.getStockItemByBarcode(Long.parseLong(id));
            if (item != null) {
                warehouse.addItem(item.getId(), item.getName(), 0, Double.parseDouble(newPrice),true);
            } else {
                System.out.println("No stock item with id " + id);
            }
        } catch (SalesSystemException | NoSuchElementException | NumberFormatException e) {
            log.error(e.getMessage());
        }
    }

    /**
     * Method for adjusting StockItem quantity
     **/
    private void adjustQuantity(Boolean inc, String id, String quantity){
        try {
            StockItem item = warehouse.getStockItemByBarcode(Long.parseLong(id));
            if (item != null) {
                if (inc)
                    warehouse.addItem(item.getId(), item.getName(), Double.parseDouble(quantity), item.getPrice(), true);
                else
                    warehouse.addItem(item.getId(), item.getName(), Double.parseDouble(quantity), item.getPrice(), false);
            } else {
                System.out.println("No stock item with id " + id);
            }
        } catch (SalesSystemException | NoSuchElementException | NumberFormatException e) {
            log.error(e.getMessage());
        }
    }

    /**
     * Method for printing the shopping cart content
     **/
    private void showCart() {
        log.info("Show shopping cart content");
        System.out.println("-------------------------");
        for (SoldItem si : cart.getAllSoldItemsInShoppingCart()) {
            System.out.println(si.getName() + " " + (si.getPrice() * si.getQuantity()) + " eur (" + si.getQuantity() + " items)");
        }
        if (cart.getAllSoldItemsInShoppingCart().size() == 0) {
            System.out.println("\tNothing in the cart");
        } else {
            System.out.println("Total sum: " + cart.totalSum());
        }
        System.out.println("-------------------------");
    }

    /**
     * Method for printing the history
     **/
    private void showHistory(Boolean ten) {
        System.out.println("-------------------------");
        List<SoldPurchase> soldPurchases;
        if (!ten) {
            soldPurchases = history.getAllSoldPurchases();
            log.info("Showing orders history");
        }
        else {
            soldPurchases = history.getLastTenPurchases();
            log.info("Showing orders history, last 10");
        }
        if (soldPurchases.size() == 0) {
            System.out.println("---");
            System.out.println("No completed purchases");
            System.out.println("---");
            return;
        }
        printSoldPurchaseList(soldPurchases);
        System.out.println("-------------------------");
    }

    /**
     * Method for showing items with low quantity
     **/
    private void showLowQuantity() {
        log.info("Show low quantity items");
        printStockItemList(warehouse.getLowQuantityStockItems(), "No items running low in stock");
    }

    /**
     * Method for showing the team info
     **/
    private void showTeam() {
        log.info("Show team");
        System.out.println("-------------------------");

        // reading the file application.properties from main project resources folder
        String path = "" + Paths.get("./").toAbsolutePath().normalize().toString().replace("\\SaleSystemCLI", "") + "\\src\\main\\resources\\application.properties";
        try (InputStream input = new FileInputStream(path)) {

            Properties prop = new Properties();

            // load the properties file
            prop.load(input);

            // read and print the properties of team tab
            System.out.println("Team name: " + prop.getProperty("teamName"));
            System.out.println("Team leader: " + prop.getProperty("teamMember1"));
            System.out.println("Team leader email: " + prop.getProperty("leaderEmail"));
            System.out.println("Team members: " + prop.getProperty("teamMember1") + ", " + prop.getProperty("teamMember2") + ", " + prop.getProperty("teamMember3"));

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        System.out.println("-------------------------");
    }

    /**
     * Method, that shows command list
     **/
    private void printUsage() {
        log.info("Print help");
        System.out.println("-------------------------");
        System.out.println("h\t\tShow this help");
        System.out.println("-------------------------");
        System.out.println("        Purchase:        ");
        System.out.println("-------------------------");
        System.out.println("c\t\tShow cart contents");
        System.out.println("a <ID> <NR> \n\t\tAdd NR of stock item with (barcode) ID to the cart");
        System.out.println("p\t\tPurchase the shopping cart");
        System.out.println("r\t\tReset the shopping cart");
        System.out.println("r <ID> <NR>\n\t\tRemove NR of stock item with ID from the cart");
        System.out.println("-------------------------");
        System.out.println("        Warehouse:       ");
        System.out.println("-------------------------");
        System.out.println("w\t\tShow warehouse contents");
        System.out.println("s <barcode> \n\t\tSearch for the product by bar code");
        System.out.println("sn <name> \n\t\tSearch for the product by the first letters or full name (case sensitive)");
        System.out.println("l\t\tShow all products running low in quantity");
        System.out.println("a p <ID> <Price> \n\t\tAdjust the price of product with barcode ID");
        System.out.println("i q <ID> <Quantity> \n\t\tIncrease the quantity of product with index ID");
        System.out.println("d q <ID> <Quantity> \n\t\tDecrease the quantity of product with index ID");
        System.out.println("add <ID> <Name> <Price> <Amount>" + "\n\t\tAdd new StockItem to the warehouse");
        System.out.println("-------------------------");
        System.out.println("        History:         ");
        System.out.println("-------------------------");
        System.out.println("hist\tShow complete history");
        System.out.println("hist10\tShow last 10 purchases");
        System.out.println("-------------------------");
        System.out.println("          Team:          ");
        System.out.println("-------------------------");
        System.out.println("t\t\tShow team info");
        System.out.println("-------------------------");
    }

    /**
     * Method for processing commands
     **/
    private void processCommand(String command) {
        String[] c = command.split(" ");
        log.debug("User input: " + command);
        try {
            if (c[0].equals("h"))
                printUsage();
            else if (c[0].equals("q"))
                System.exit(0);
            else if (c[0].equals("w"))
                showStock();
            else if (c[0].equals("l"))
                showLowQuantity();
            else if (c[0].equals("c"))
                showCart();
            else if (c[0].equals("p"))
                purchaseCart();
            else if (c[0].equals("r") && c.length == 3)
                removeFromCart(c[1], c[2]);
            else if (c[0].equals("r"))
                cart.cancelCurrentPurchase();
            else if (c[0].equals("hist"))
                showHistory(false);
            else if (c[0].equals("hist10"))
                showHistory(true);
            else if (c[0].equals("a") && c.length == 3) {
                addToCart(c[1], c[2]);
            } else if (c[0].equals("a") && c[1].equals("p") && c.length == 4) {
                adjustPrice(c[2],c[3]);
            } else if (c[0].equals("i") && c[1].equals("q") && c.length == 4) {
                adjustQuantity(true, c[2], c[3]);
            } else if (c[0].equals("d") && c[1].equals("q") && c.length == 4) {
                adjustQuantity(false, c[2], c[3]);
            } else if (c[0].equals("add") && c.length == 5) {
                addNewStockItem(c[1], c[2], c[3], c[4]);
            } else if (c[0].equals("t")) {
                showTeam();
            } else if (c[0].equals("s")) {
                findStock(c[1]);
            } else if (c[0].equals("sn")) {
                findStockName(c[1]);
            } else {
                System.out.println("unknown command");
            }
        } catch (
                ArrayIndexOutOfBoundsException e) {
            System.out.println("incomplete command");
        }
    }

}

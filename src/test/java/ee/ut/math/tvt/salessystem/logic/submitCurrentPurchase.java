package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.Category;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.SoldPurchase;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mockito;

import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;

public class submitCurrentPurchase {
    //Not sure if I can use InMemorySalesSystemDAO here
    private SalesSystemDAO mocked = Mockito.spy(new InMemorySalesSystemDAO());

    @Test
    public void testSubmittingCurrentPurchaseDecreasesStockItemQuantity() throws QuantityException {
        ShoppingCart cart = new ShoppingCart(mocked);
        StockItem testItem = new StockItem(10L, "TestItem", "This is for testing purposes", 150, 5, 2, new Category(4L, "Test", true));
        mocked.saveStockItem(testItem);
        cart.addSoldItem("10", "2", false);
        cart.submitCurrentPurchase();
        double expected = 3;
        double actual = testItem.getQuantity();
        assertEquals(expected, actual, 0);
    }

    @Test
    public void testSubmittingCurrentPurchaseBeginsAndCommitsTransaction() throws QuantityException {
        ShoppingCart cart = new ShoppingCart(mocked);
        StockItem testItem = new StockItem(10L, "TestItem", "This is for testing purposes", 150, 5, 2, new Category(4L, "Test", true));
        mocked.saveStockItem(testItem);
        cart.addSoldItem("10", "2", false);
        cart.submitCurrentPurchase();
        InOrder inOrder = inOrder(mocked);
        inOrder.verify(mocked, times(1)).beginTransaction();
        inOrder.verify(mocked, times(1)).commitTransaction();
    }

    @Test
    public void testSubmittingCurrentOrderCreatesHistoryItem() throws QuantityException {
        ShoppingCart cart = new ShoppingCart(mocked);
        List<SoldPurchase> CurrentSoldPurchases= mocked.findSoldPurchases();
        int currentSize = CurrentSoldPurchases.size();
        cart.addSoldItem("1", "2", false);
        cart.addSoldItem("2", "1", false);

        cart.submitCurrentPurchase();
        int newSize = CurrentSoldPurchases.size();
        SoldPurchase purchase = CurrentSoldPurchases.get(0);

        //Sold purchases has been updated.
        assertNotEquals(currentSize, newSize);

        //Verifying that items in history are the same as our added items.
        //Would use assertEquals() to compare items but it fails even though items are the same, compared most important values instead.
        List<SoldItem> purchasedItems= mocked.findSoldPurchaseContent(purchase);
        //Comparing names
        assertEquals(purchasedItems.get(0).getName(), cart.getStockItem((purchasedItems.get(0).getBarcode()).toString()).getName());
        assertEquals(purchasedItems.get(1).getName(), cart.getStockItem((purchasedItems.get(1).getBarcode()).toString()).getName());
        //Comparing quantities
        assertEquals(2, purchasedItems.get(0).getQuantity(), 0);
        assertEquals(1, purchasedItems.get(1).getQuantity(), 0);
    }

    @Test
    public void testSubmittingCurrentOrderSavesCorrectTime() throws QuantityException {
        ShoppingCart cart = new ShoppingCart(mocked);
        cart.addSoldItem("1", "2", false);
        cart.submitCurrentPurchase();

        //Comparing our recorded time and saved time to see if they are close enough
        assertTrue(Math.abs(new Date().getTime() - mocked.findSoldPurchases().get(0).getDate().getTime()) < 1000);
    }

    @Test
    public void testCancellingOrder() throws QuantityException {
        ShoppingCart cart = new ShoppingCart(mocked);
        Warehouse warehouse = new Warehouse(mocked);
        StockItem testItem1 = new StockItem(10L, "TestItem", "This is for testing purposes", 150, 5, 2, new Category(4L, "Test", true));
        StockItem testItem2 = new StockItem(11L, "TestItem_2", "This is for testing purposes", 150, 10, 2, new Category(4L, "Test", true));
        StockItem testItem3 = new StockItem(12L, "TestItem_3", "This is for testing purposes", 150, 2, 2, new Category(4L, "Test", true));
        warehouse.addItem(testItem1);
        warehouse.addItem(testItem2);
        warehouse.addItem(testItem3);
        cart.addSoldItem("10", "4", false);
        cart.addSoldItem("11", "8", false);
        cart.addSoldItem("12", "1", false);
        cart.cancelCurrentPurchase();
        StockItem testItem4 = new StockItem(13L, "TestItem_4", "This is for testing purposes", 150, 15, 2, new Category(4L, "Test", true));
        warehouse.addItem(testItem4);
        cart.addSoldItem("13", "2.0", false);
        List<SoldItem> actual = cart.getAllSoldItemsInShoppingCart();
        List<SoldItem> expected = new ArrayList<>();
        expected.add(new SoldItem(testItem4, 2.0));

        //Converted to String to compare
        assertEquals(expected.toString(), actual.toString());
        //Making sure that the items are the same
        assertEquals(expected.get(0).toString(), actual.get(0).toString());
    }

    @Test
    public void testCancellingOrderQuantitiesUnchanged() throws QuantityException {
        ShoppingCart cart = new ShoppingCart(mocked);
        StockItem testItem = new StockItem(10L, "TestItem", "This is for testing purposes", 150, 5, 2, new Category(4L, "Test", true));
        mocked.saveStockItem(testItem);
        double expectedQuantity = testItem.getQuantity();
        cart.addSoldItem("10", "2", false);
        cart.cancelCurrentPurchase();
        double actualQuantity = testItem.getQuantity();
        assertEquals(expectedQuantity, actualQuantity, 0);
    }
}
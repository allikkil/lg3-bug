package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.Category;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.*;
import static org.mockito.Mockito.spy;

public class addItem {

    private SalesSystemDAO spy = spy(new InMemorySalesSystemDAO());

    @Test
    public void testAddingExistingItem() throws QuantityException {
        ShoppingCart cart = new ShoppingCart(spy);
        Warehouse warehouse = new Warehouse(spy);
        StockItem testItem = new StockItem(10L, "TestItem", "This is for testing purposes", 150, 5, 2, new Category(4L, "Test", true));
        warehouse.addItem(testItem);
        cart.addSoldItem("10", "1", false);
        cart.addSoldItem("10", "2", false);
        double expected = 3;
        double actual = cart.getAllSoldItemsInShoppingCart().get(0).getQuantity();
        assertEquals(expected, actual, 0);
    }

    @Test
    public void testAddingNewItem() throws QuantityException {
        ShoppingCart cart = new ShoppingCart(spy);
        Warehouse warehouse = new Warehouse(spy);
        StockItem testItem = new StockItem(10L, "TestItem", "This is for testing purposes", 150, 5, 2, new Category(4L, "Test", true));
        warehouse.addItem(testItem);
        cart.addSoldItem("10", "2", false);
        int afterAddingSize = cart.getAllSoldItemsInShoppingCart().size();
        SoldItem expected = new SoldItem(testItem, 2);
        SoldItem actual = cart.getAllSoldItemsInShoppingCart().get(afterAddingSize - 1);

        assertEquals(expected.toString(), actual.toString());


    }

    @Test
    public void testAddingItemWithNegativeQuantity() {
        ShoppingCart cart = new ShoppingCart(spy);

        assertThrows(QuantityException.class, () -> {
            cart.addSoldItem("10", "-1", false);
        });
    }

    @Test
    public void testAddingItemWithQuantityTooLarge() {
        ShoppingCart cart = new ShoppingCart(spy);
        Warehouse warehouse = new Warehouse(spy);
        StockItem testItem = new StockItem(10L, "TestItem", "This is for testing purposes", 150, 5, 2, new Category(4L, "Test", true));
        warehouse.addItem(testItem);
        assertThrows(QuantityException.class, () -> {
            cart.addSoldItem("10", "6", true);
        });
    }

    @Test
    public void testAddingItemWithQuantitySumTooLarge() throws QuantityException {
        ShoppingCart cart = new ShoppingCart(spy);
        Warehouse warehouse = new Warehouse(spy);
        StockItem testItem = new StockItem(10L, "TestItem", "This is for testing purposes", 150, 5, 2, new Category(4L, "Test", true));
        warehouse.addItem(testItem);
        cart.addSoldItem("10", "4", true);
        assertThrows(QuantityException.class, () -> {
            cart.addSoldItem("10", "2", true);
        });
    }
}
package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.Category;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import org.junit.Test;
import org.mockito.InOrder;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

public class WarehouseAddItem {

    @Test
    public void testAddingItemBeginsAndCommitsTransaction() {
        InMemorySalesSystemDAO dao_spy = spy(new InMemorySalesSystemDAO());
        Warehouse warehouse = new Warehouse(dao_spy);
        StockItem item = new StockItem(10L, "TestItem", "This is for testing purposes", 150, 5, 2, new Category(4L, "Test", true));
        warehouse.addItem(item);
        InOrder inOrder = inOrder(dao_spy);
        inOrder.verify(dao_spy, times(1)).beginTransaction();
        inOrder.verify(dao_spy, times(1)).commitTransaction();
    }

    @Test
    public void testAddingNewItem() {
        InMemorySalesSystemDAO dao = new InMemorySalesSystemDAO();
        Warehouse warehouse = new Warehouse(dao);
        StockItem item = new StockItem(10L, "TestItem", "This is for testing purposes", 150, 5, 2, new Category(4L, "Test", true));
        warehouse.addItem(item);
        StockItem val = warehouse.getStockItemByBarcode(10L);
        assertEquals(item, val);
    }

    @Test
    public void testAddingExistingItemIncreaseQuantity() {
        InMemorySalesSystemDAO dao_spy = spy(new InMemorySalesSystemDAO());
        Warehouse warehouse = new Warehouse(dao_spy);
        StockItem existing = warehouse.getStockItemByBarcode(1L);
        double existingQuantity = existing.getQuantity();
        //Increasing quantity by 12
        warehouse.addItem(existing.getId(), existing.getName(), 12, existing.getPrice(), true);
        StockItem changed = warehouse.getStockItemByBarcode(existing.getId());
        double newQuantity = changed.getQuantity();
        double decreasedVal = existingQuantity + 12;
        assertEquals(decreasedVal, newQuantity, 0);
        verify(dao_spy, never()).saveStockItem(any());
    }

    @Test
    public void testAddingExistingItemDecreaseQuantity() {
        InMemorySalesSystemDAO dao_spy = spy(new InMemorySalesSystemDAO());
        Warehouse warehouse = new Warehouse(dao_spy);
        StockItem existing = warehouse.getStockItemByBarcode(1L);
        double existingQuantity = existing.getQuantity();
        //Decreasing quantity by 1
        warehouse.addItem(existing.getId(), existing.getName(), 1, existing.getPrice(), false);
        StockItem changed = warehouse.getStockItemByBarcode(existing.getId());
        double newQuantity = changed.getQuantity();
        double decreasedVal = existingQuantity - 1;
        assertEquals(decreasedVal, newQuantity, 0);
        verify(dao_spy, never()).saveStockItem(any());
    }

    @Test
    public void testAddingItemWithNegativeQuantity() {
        InMemorySalesSystemDAO dao = new InMemorySalesSystemDAO();
        Warehouse warehouse = new Warehouse(dao);
        StockItem itemNegativeQuantity = new StockItem(10L, "TestItem", "This is for testing purposes", 150, -5, 2, new Category(4L, "Test", true));
        assertThrows(IllegalArgumentException.class, () -> {
            warehouse.addItem(itemNegativeQuantity);
        });

    }

}
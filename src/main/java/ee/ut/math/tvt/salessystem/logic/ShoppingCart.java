package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.SoldPurchase;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Encapsulates everything that has to do with selling.
 * All the logic behind the point of sale is in this class.
 **/

public class ShoppingCart {

    private static final Logger log = LogManager.getLogger(ShoppingCart.class);
    private final SalesSystemDAO dao;
    private final List<SoldItem> SoldItemsInShoppingCart = new ArrayList<>();
    private double discount;

    public ShoppingCart(SalesSystemDAO dao) {
        this.dao = dao;
    }

    /**
     * Add new SoldItem to ShoppingCart.
     **/
    public void addSoldItem(String stringBarcode, String stringQuantity, boolean throwException) throws QuantityException {
        StockItem stockItem;
        double quantity;
        long barcode;

        try {
            barcode = Long.parseLong(stringBarcode);
            quantity = Double.parseDouble(stringQuantity);
            stockItem = dao.findStockItem(barcode);
        } catch (NumberFormatException e) {
            throw new NumberFormatException("Barcode and quantity must be numeric");
        }

        log.debug("Verifying that warehouse items quantity remains at least zero or throw an exception");
        if (quantity < 0) {
            throw new QuantityException("Quantity must be positive");
        } else if (quantity > stockItem.getQuantity()) {
            if (throwException) {
                throw new QuantityException("Stock quantity exceeded");
            }
            quantity = stockItem.getQuantity();
        }

        // see if item is already in the shopping cart
        for (SoldItem item : SoldItemsInShoppingCart) {
            if (item.getBarcode().equals(barcode) && quantity + item.getQuantity() < stockItem.getQuantity()) {
                item.setQuantity(item.getQuantity() + quantity);
                log.debug("Found item in the shopping cart and added " + quantity + " to the quantity");
                return;
            } else if (item.getBarcode().equals(barcode)) {
                if (throwException) {
                    throw new QuantityException("Stock quantity exceeded");
                }
                item.setQuantity(item.getQuantity() + (stockItem.getQuantity() - item.getQuantity()));
                return;
            }
        }

        SoldItemsInShoppingCart.add(new SoldItem(stockItem, quantity));
        log.info("Added " + stockItem.getName() + " quantity of " + quantity);
    }

    public void removeSoldItem(String stringBarcode, String stringQuantity) throws QuantityException {
        double quantity;
        long barcode;

        try {
            barcode = Long.parseLong(stringBarcode);
            quantity = Double.parseDouble(stringQuantity);
        } catch (NumberFormatException e) {
            throw new NumberFormatException("Quantity must be numeric");
        }

        // see if item is already in the shopping cart
        for (SoldItem item : SoldItemsInShoppingCart) {
            if (item.getBarcode().equals(barcode)) {
                if (item.getQuantity() > quantity) {
                    item.setQuantity(item.getQuantity() - quantity);
                } else if (item.getQuantity() < quantity)
                    throw new QuantityException("Inserted quantity exceeded the quantity in the shopping cart");
                else if (item.getQuantity() == quantity) {
                    SoldItemsInShoppingCart.remove(item);
                    log.debug("Removed " + item.getStockItem().getName() + " quantity of " + item.getQuantity());
                }
                log.debug("Found the item in the shopping cart");
                return;
            }
        }
        log.debug("Did not find the item in the shopping cart");
        throw new QuantityException("Cannot remove an item if the item is not in the shopping cart");
    }


    public List<SoldItem> getAllSoldItemsInShoppingCart() {
        log.debug("Returning the content of shopping cart");
        return SoldItemsInShoppingCart;
    }

    public void cancelCurrentPurchase() {
        log.info("Clearing shopping cart");
        SoldItemsInShoppingCart.clear();
    }

    public void submitCurrentPurchase() {
        SoldPurchase soldPurchase = new SoldPurchase(SoldItemsInShoppingCart, discount);

        for (SoldItem item : SoldItemsInShoppingCart) {
            StockItem i = dao.findStockItem(item.getStockItem().getId());
            i.setQuantity(i.getQuantity() - item.getQuantity());
        }
        log.info("Decreased quantities of the warehouse stock after submitting purchase.");

        dao.saveSoldPurchase(soldPurchase);

        try {
            dao.beginTransaction();
            for (SoldItem item : SoldItemsInShoppingCart) {
                item.setSoldPurchase(soldPurchase);
                dao.saveSoldItem(item);
            }
            dao.commitTransaction();
            SoldItemsInShoppingCart.clear();
        } catch (Exception e) {
            dao.rollbackTransaction();
            throw e;
        }
    }

    /**
     * Calculate total sum of the products in the cart.
     **/
    public double totalSum() {
        double sum = 0;
        for (SoldItem item : SoldItemsInShoppingCart) {
            sum += item.getSum();
        }
        log.debug("Returning the total sum");
        return sum * (100 - discount) / 100;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        double discountDouble;
        try {
            discountDouble = Double.parseDouble(discount);
            if (discountDouble <= 0 || discountDouble > 100) {
                throw new IllegalArgumentException("Discount can not be negative nor greater than 100%");
            }
        } catch (NumberFormatException e) {
            throw new NumberFormatException("Discount must be numeric");
        }
        this.discount = discountDouble;
    }

    public String getStockItemQuantity(String stringBarcode) {
        try {
            long barcode = Long.parseLong(stringBarcode);
            return String.valueOf(dao.findStockItem(barcode).getQuantity());
        } catch (NumberFormatException e) {
            throw new NumberFormatException("Quantity must be numeric");
        }
    }

    public StockItem getStockItem(String stringBarcode) {
        try {
            long barcode = Long.parseLong(stringBarcode);
            if (barcode < 0) {
                throw new NumberFormatException("Barcode must be positive");
            }
            StockItem stockItem = dao.findStockItem(barcode);
            if (stockItem == null) {
                throw new IllegalArgumentException("Stockitem not found");
            }
            return stockItem;
        } catch (NumberFormatException e) {
            throw new NumberFormatException("Barcode must be numeric");
        }
    }
}

package ee.ut.math.tvt.salessystem.dataobjects;

import javax.persistence.*;
import java.util.List;

/**
 * Stock item for keeping the warehouse.
 **/

@Entity
@Table(name = "STOCKITEM")
public class StockItem {

    @Id
    private Long id;
    @Column(name = "Name")
    private String name;
    @Column(name = "Price")
    private double price;
    @Column(name = "Description")
    private String description;
    @Column(name = "Quantity")
    private double quantity;
    private double lowQuantity;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "Category_id")
    private Category category;
    @OneToMany(mappedBy = "stockItem", fetch = FetchType.EAGER)
    private List<SoldItem> soldItems;
    private String islowquantity;

    public StockItem() {
    }

    public StockItem(Long id, String name, String desc, double price, double quantity, double lowQuantity, Category category) {
        this.id = id;
        this.name = name;
        this.description = desc;
        this.price = price;
        this.quantity = quantity;
        this.lowQuantity = lowQuantity;
        this.category = category;
        if (quantity < lowQuantity) {
            this.islowquantity = "Yes (Order: " + (lowQuantity - quantity) + ")";
        } else {
            this.islowquantity = "";
        }
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        if (price > 0) {
            this.price = price;
        } else {
            throw new NumberFormatException("Price can not be negative");
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        if (quantity >= 0) {
            this.quantity = quantity;
            if (quantity < this.lowQuantity) {
                this.islowquantity = "Yes (Order: " + (lowQuantity - quantity) + ")";
            } else {
                this.islowquantity = "";
            }
        } else {
            throw new NumberFormatException("Can not set quantity to be negative");
        }
    }

    public void increaseQuantity(double quantity) {
        if (quantity >= 0) {
            this.setQuantity(this.getQuantity() + quantity);
        } else {
            throw new NumberFormatException("Can not increase by negative quantity");
        }
    }

    public void decreaseQuantity(double quantity) {
        if (quantity >= 0) {
            this.setQuantity(this.getQuantity() - quantity);
        } else {
            throw new NumberFormatException("Can not decrease by negative quantity");
        }
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public double getLowQuantity() {
        return lowQuantity;
    }

    public String getIslowquantity() {
        return islowquantity;
    }

    public void setLowQuantity(double lowQuantity) {
        this.lowQuantity = lowQuantity;
    }

    public void setIslowquantity(String islowquantity) {
        this.islowquantity = islowquantity;
    }

    public List<SoldItem> getSoldItems() {
        return soldItems;
    }

    @Override
    public String toString() {
        return String.format("StockItem: id: %d, name: '%s'}", id, name);
    }
}

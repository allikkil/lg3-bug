package ee.ut.math.tvt.salessystem.dataobjects;

import javax.persistence.*;

/**
 * Already bought StockItem. SoldItem duplicates name and price for preserving history.
 **/

@Entity
@Table(name = "SOLDITEM")
public class SoldItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Transient
    private Long barcode;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "StockItem_id")
    private StockItem stockItem;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "SoldPurchase_id")
    private SoldPurchase soldPurchase;
    @Column(name = "Quantity")
    private double quantity;
    @Column(name = "Price")
    private double price;

    public SoldItem() {
    }

    public SoldItem(StockItem stockItem, double quantity) {
        this.stockItem = stockItem;
        this.barcode = stockItem.getId();
        this.price = stockItem.getPrice();
        this.quantity = quantity;
    }

    public Long getId() {
        return id;
    }

    public Long getBarcode() {
        return getStockItem().getId();
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getPrice() {
        return price;
    }

    public String getName() {
        return stockItem.getName();
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public double getSum() {
        return price * quantity;
    }

    public StockItem getStockItem() {
        return stockItem;
    }

    public void setStockItem(StockItem stockItem) {
        this.stockItem = stockItem;
    }

    public SoldPurchase getSoldPurchase() {
        return soldPurchase;
    }

    public void setSoldPurchase(SoldPurchase soldPurchase) {
        this.soldPurchase = soldPurchase;
    }

    @Override
    public String toString() {
        return "SoldItem ID: " + getId() + " | Name: " + getName() + " | Quantity: " + getQuantity();
    }
}

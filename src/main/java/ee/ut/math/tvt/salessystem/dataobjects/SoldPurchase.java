package ee.ut.math.tvt.salessystem.dataobjects;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

/**
 * Already sold purchase. Contains sold purchase info for preserving history.
 * Info includes date, time, id, total price, total quantity and all the sold items of the sale.
 **/

@Entity
@Table(name = "SOLDPURCHASE")
public class SoldPurchase {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "Date")
    private Date date;
    private LocalDate localDate;
    private double discount;
    @Column(name = "Total_Price")
    private double totalPrice;
    @Column(name = "Total_Quantity")
    private double totalQuantity;
    @OneToMany(mappedBy = "soldPurchase", fetch = FetchType.EAGER)
    private List<SoldItem> soldItems;

    public SoldPurchase() {
    }

    public SoldPurchase(List<SoldItem> items, double discount) {
        double price = 0;
        double quantity = 0;

        this.discount = discount;
        this.date = new Date();
        this.localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        this.soldItems = items;
        for (SoldItem item : items) {
            price += item.getSum();
            quantity += item.getQuantity();
        }
        this.totalPrice = price;
        this.totalQuantity = quantity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public LocalDate getLocalDate() {
        return localDate;
    }

    public void setLocalDate(LocalDate localDate) {
        this.localDate = localDate;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public double getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(double totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public List<SoldItem> getItems() {
        return soldItems;
    }

    public void setItems(List<SoldItem> soldItems) {
        this.soldItems = soldItems;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public List<SoldItem> getSoldItems() {
        return soldItems;
    }

    public void setSoldItems(List<SoldItem> soldItems) {
        this.soldItems = soldItems;
    }

    @Override
    public String toString() {
        return "Purchase number: " + getId() + "\t\tDate and Time: " + getDate() +
                "\t\tTotal price: " + getTotalPrice() + "\t\tTotal quantity: " + getTotalQuantity();
    }
}

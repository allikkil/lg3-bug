package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Encapsulates everything that has to do with the warehouse.
 * All the logic behind the warehouse is in this class.
 **/

public class Warehouse {

    private static final Logger log = LogManager.getLogger(Warehouse.class);
    private final SalesSystemDAO dao;

    public Warehouse(SalesSystemDAO dao) {
        this.dao = dao;
    }

    public void addItem(StockItem stockItem) {
        dao.saveStockItem(stockItem);
    }

    /**
     * Add new StockItem to the Warehouse.
     **/
    public void addItem(long barcode, String name, double quantity, double price, boolean increase) {
        StockItem item = dao.findStockItem(barcode);
        if (name.equals("")) {
            throw new IllegalArgumentException("Name field can not be empty");
        }
        if (item != null) {
            if (!increase && item.getQuantity() - quantity < 0) {
                throw new IllegalStateException("You can not decrease the quantity by more than what's in stock");
            }
            dao.updateExistingStockItem(barcode, quantity, price, increase);
            log.info("Updated StockItem " + item.toString() + " info in the warehouse");
        } else {
            StockItem newItem = new StockItem(barcode, name, "", price, quantity, 10, dao.findCategory(5L));
            dao.saveStockItem(newItem);
            log.info("Saved StockItem " + newItem.toString() + " to the warehouse");
        }
    }

    /**
     * Remove existing StockItem from the Warehouse.
     **/
    public void removeItem(long barcode) {
        if (barcode < 0) {
            throw new IllegalArgumentException("Barcode can not be negative");
        }
        StockItem item = dao.findStockItem(barcode);
        if (item != null) {
            dao.removeStockItem(barcode);
            log.info("Removed StockItem with ID=" + barcode + " from the warehouse");
        } else {
            throw new IllegalStateException("Product not found");
        }
    }

    /**
     * Search StockItem by barcode.
     **/
    public List<StockItem> searchStockItemByBarcode(String id) {
        long barcode;
        try {
            barcode = Long.parseLong(id);
        } catch (NumberFormatException e) {
            throw new NumberFormatException("Input must be numeric when searching by ID");
        }
        if (barcode < 0) {
            throw new NumberFormatException("There are no products with negative barcode. Try a positive barcode.");
        }
        StockItem itemById = dao.findStockItem(barcode);
        List<StockItem> stockItems = new ArrayList<>();
        if (itemById != null) {
            stockItems = Collections.singletonList(itemById);
        }
        return stockItems;
    }

    /**
     * Search StockItems by name.
     **/
    public List<StockItem> searchStockItemByName(String name) {
        return dao.findStockItem_name(name);
    }

    /**
     * Search StockItems by category.
     **/
    public List<StockItem> searchStockItemByCategory(String category) {
        return dao.findStockItem_category(category);
    }

    /**
     * Get list of all warehouse items.
     **/
    public List<StockItem> getAllStockItems() {
        return dao.findStockItems();
    }

    /**
     * Get a list of low quantity warehouse items.
     **/
    public List<StockItem> getLowQuantityStockItems() {
        return dao.findStockItemsLowQuantity();
    }

    /**
     * Get StockItem by barcode.
     **/
    public StockItem getStockItemByBarcode(long barcode) {
        return dao.findStockItem(barcode);
    }
}

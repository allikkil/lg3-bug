package ee.ut.math.tvt.salessystem.dataobjects;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "CATEGORY")
public class Category {

    @Id
    private Long id;
    @Column(name = "Name")
    private String name;
    @Column(name = "Restriction")
    private Boolean restriction;
    @Column(name = "Weighable")
    private Boolean weighable;
    @OneToMany(mappedBy = "category", fetch = FetchType.EAGER)
    private Set<StockItem> stockItems;

    public Category() {
    }

    public Category(Long id, String name, Boolean restriction) {
        this.id = id;
        this.name = name;
        this.restriction = restriction;
        this.weighable = false;
    }

    public Category(Long id, String name, Boolean restriction, Boolean weighable) {
        this.id = id;
        this.name = name;
        this.restriction = restriction;
        this.weighable = weighable;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getRestriction() {
        return restriction;
    }

    public void setRestriction(Boolean restriction) {
        this.restriction = restriction;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<StockItem> getStockItems() {
        return stockItems;
    }

    @Override
    public String toString() {
        return name;
    }
}

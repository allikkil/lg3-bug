package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.SoldPurchase;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.time.LocalDate;
import java.util.List;

/**
 * Encapsulates everything that has to do with the history.
 * All the logic behind the history is in this class.
 **/

public class History {

    private static final Logger log = LogManager.getLogger(History.class);
    private final SalesSystemDAO dao;
    private final Warehouse warehouse;

    public History(SalesSystemDAO dao, Warehouse warehouse) {
        this.dao = dao;
        this.warehouse = warehouse;
    }

    /**
     * Get list of all SoldPurchases.
     **/
    public List<SoldPurchase> getAllSoldPurchases() {
        return dao.findSoldPurchases();
    }

    /**
     * Get list of last ten SoldPurchases.
     **/
    public List<SoldPurchase> getLastTenPurchases() {
        return dao.findSoldPurchase_lastTen();
    }


    /**
     * Get list of SoldPurchases sold between dates (start - end).
     **/
    public List<SoldPurchase> getSoldPurchasesBetweenDates(LocalDate start, LocalDate end) {
        if (start != null && end != null) {
            return dao.findSoldPurchase_betweenDates(start, end);
        } else {
            throw new IllegalArgumentException("Please fill both date fields");
        }
    }

    /**
     * Get list of SoldItems for given SoldPurchase.
     **/
    public List<SoldItem> getAllSoldItemsOfSelectedSoldPurchase(SoldPurchase soldPurchase) {
        return dao.findSoldPurchaseContent(soldPurchase);
    }

    /**
     * Get list of SoldItems for given SoldPurchase.
     **/
    public List<SoldItem> getAllSoldItems() {
        return dao.findSoldItems();
    }


    /**
     * Return SoldItem.
     **/
    public void returnSoldItem(SoldPurchase soldPurchase, SoldItem soldItem, String stringQuantity) throws QuantityException {
        try {
            double quantity = Double.parseDouble(stringQuantity);
            if (quantity < 0) {
                // if inserted quantity is negative, throw exception
                throw new IllegalStateException("Quantity must be positive");
            }
            if (quantity > soldItem.getQuantity()) {
                // if inserted quantity is larger than SoldItem quantity
                throw new IllegalStateException("Can not return more items than sold");
            }
            double newSelectedItemQuantity = soldItem.getQuantity() - quantity;
            if (newSelectedItemQuantity == 0) {
                soldPurchase.getSoldItems().remove(soldItem);
                dao.removeSoldItem(soldItem);
                log.info("Removed SoldItem ID=" + soldItem.getBarcode() + " from SoldPurchase");
            } else {
                soldItem.setQuantity(newSelectedItemQuantity);
                dao.updateSoldItem(soldItem);
                log.info("Set SoldItem ID=" + soldItem.getBarcode() + " quantity to " + newSelectedItemQuantity);
            }
            StockItem stockItem = dao.findStockItem(soldItem.getBarcode());
            if (stockItem == null) {
                warehouse.addItem(soldItem.getBarcode(), soldItem.getName(), quantity, soldItem.getPrice(), true);
                log.info("Added new StockItem ID=" + soldItem.getBarcode() + " with quantity " + quantity);
            } else {
                dao.updateExistingStockItem(stockItem.getId(), quantity, stockItem.getPrice(), true);
                log.info("Increased StockItem ID=" + soldItem.getBarcode() + " quantity by " + quantity);
            }
            dao.updateSoldPurchase(soldPurchase, soldPurchase.getTotalQuantity() - quantity,
                    soldPurchase.getTotalPrice() - quantity * soldItem.getPrice());
            log.info("Updated SoldPurchase ID=" + soldPurchase.getId() + " total quantity and price");
        } catch (NumberFormatException e) {
            throw new QuantityException("Quantity must be numeric");
        } catch (IllegalStateException e) {
            throw new QuantityException(e.getMessage());
        }
    }

}

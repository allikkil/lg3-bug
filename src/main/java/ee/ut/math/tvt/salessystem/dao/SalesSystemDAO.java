package ee.ut.math.tvt.salessystem.dao;

import ee.ut.math.tvt.salessystem.dataobjects.Category;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.SoldPurchase;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;

import java.time.LocalDate;
import java.util.List;

/**
 * Data Access Object for loading and saving sales system data.
 * Feel free to add methods here!
 * <p>
 * The sample implementation is called {@link InMemorySalesSystemDAO}. It allows you
 * to run the application without configuring a real database. Making changes is simple
 * and all data is lost when restarting the app. Later you will need to create a new
 * implementation {@link HibernateSalesSystemDAO} for SalesSystemDAO that uses a real
 * database to store the data. Keep the existing InMemorySalesSystemDAO implementation,
 * it will be useful when writing tests in lab6.
 * <p>
 * Implementations of this class must only handle storage/retrieval of the data.
 * Business logic and validation should happen in separate specialized classes.
 * Separating data access and business logic allows you to later test the business
 * logic using the InMemorySalesSystemDAO and avoid configuring the database for
 * each test (much faster and more convenient).
 * <p>
 * Note the transaction related methods. These will become relevant when you
 * start using a real database. Transactions allow you to group database operations
 * so that either all of them succeed or nothing at all is done.
 */
public interface SalesSystemDAO {

    List<StockItem> findStockItems();

    List<SoldItem> findSoldItems();

    StockItem findStockItem(long id);

    Category findCategory(long id);

    List<StockItem> findStockItem_name(String name);

    List<StockItem> findStockItemsLowQuantity();

    List<StockItem> findStockItem_category(String name);

    void saveStockItem(StockItem stockItem);

    void updateExistingStockItem(long id, double quantity, double price, boolean increaseQuantity);

    void removeStockItem(long id);

    void saveSoldItem(SoldItem item);

    void removeSoldItem(SoldItem item);

    void updateSoldItem(SoldItem item);

    void saveSoldPurchase(SoldPurchase soldPurchase);

    void updateSoldPurchase(SoldPurchase soldPurchase, double totalQuantity, double totalPrice);

    List<SoldPurchase> findSoldPurchases();

    List<SoldPurchase> findSoldPurchase_lastTen();

    List<SoldPurchase> findSoldPurchase_betweenDates(LocalDate begin, LocalDate end);

    List<SoldItem> findSoldPurchaseContent(SoldPurchase sp);

    void beginTransaction();

    void rollbackTransaction();

    void commitTransaction();
}

package ee.ut.math.tvt.salessystem.dao;

import ee.ut.math.tvt.salessystem.dataobjects.Category;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.SoldPurchase;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Encapsulates everything that has to do with the database
 * All the reading from and writing to database happens in this class.
 **/

public class HibernateSalesSystemDAO implements SalesSystemDAO {

    private final EntityManagerFactory emf;
    private final EntityManager em;

    public HibernateSalesSystemDAO() {
        // if you get ConnectException/JDBCConnectionException then you
        // probably forgot to start the database before starting the application
        emf = Persistence.createEntityManagerFactory("pos");
        em = emf.createEntityManager();

        /*
        List<Category> categories = new ArrayList<Category>();
        categories.add(new Category(1L, "Chips", false));
        categories.add(new Category(2L, "Sweets", false));
        categories.add(new Category(3L, "Sausages", false));
        categories.add(new Category(4L, "Beer/Ale/Alcoholic Cider", true));
        categories.add(new Category(5L, "-", false));

        saveStockItem(new StockItem(1L, "Lays chips", "Potato chips", 11.0, 5, 10, categories.get(0)));
        saveStockItem(new StockItem(2L, "Chupa-chups", "Sweets", 8.0, 8, 10, categories.get(1)));
        saveStockItem(new StockItem(3L, "Frankfurters", "Beer sausages", 15.0, 12, 10, categories.get(2)));
        saveStockItem(new StockItem(4L, "Free Beer", "Student's delight", 0.0, 100, 50, categories.get(3)));

         */
    }

    public void close() {
        em.close();
        emf.close();
    }

    /**
     * Method for getting all of the StockItems (as a list) from the database.
     **/
    @Override
    public List<StockItem> findStockItems() {
        return em.createQuery("Select a FROM StockItem a", StockItem.class).getResultList();
    }

    /**
     * Method for getting all of the StockItems (as a list) from the database.
     **/
    @Override
    public List<SoldItem> findSoldItems() {
        return em.createQuery("Select a FROM SoldItem a", SoldItem.class).getResultList();
    }

    /**
     * Method for getting the StockItem with selected ID from the database.
     **/
    @Override
    public StockItem findStockItem(long id) {
        return em.find(StockItem.class, id);
    }

    /**
     * Method for getting the StockItems with selected Category from the database.
     **/
    @Override
    public Category findCategory(long id) {
        return em.find(Category.class, id);
    }

    /**
     * Method for getting all of the StockItems with names starting with given substring (as a list) from the database
     **/
    @Override
    public List<StockItem> findStockItem_name(String name) {
        String call = "Select a FROM StockItem a where a.name like '" + name + "%'";
        return em.createQuery(call, StockItem.class).getResultList();
    }

    @Override
    public List<StockItem> findStockItemsLowQuantity() {
        String call = "Select a FROM StockItem a where a.quantity <= a.lowQuantity";
        return em.createQuery(call, StockItem.class).getResultList();
    }

    /**
     * Method for getting all of the StockItems with categories starting with given substring (as a list) from the database
     **/
    @Override
    public List<StockItem> findStockItem_category(String name) {
        String call = "Select a FROM StockItem a where a.category.name like '" + name + "%'";
        return em.createQuery(call, StockItem.class).getResultList();
    }

    /**
     * Method for saving given StockItem to the database
     **/
    @Override
    public void saveStockItem(StockItem stockItem) {
        if (stockItem.getPrice() < 0 || stockItem.getQuantity() < 0) {
            throw new IllegalArgumentException();
        } else {
            beginTransaction();
            em.persist(stockItem);
            commitTransaction();
        }
    }

    /**
     * Method for updating given StockItem's info in the database
     **/
    public void updateExistingStockItem(long id, double quantity, double price, boolean increaseQuantity) {
        StockItem item = em.find(StockItem.class, id);
        if (item != null) {
            if (increaseQuantity) {
                item.increaseQuantity(quantity);
            } else {
                item.decreaseQuantity(quantity);
            }
            item.setPrice(price);
            beginTransaction();
            em.merge(item);
            commitTransaction();
        }
    }

    /**
     * Method for removing StockItem with given ID from the database
     **/
    @Override
    public void removeStockItem(long id) {
        StockItem item = em.find(StockItem.class, id);
        if (item != null) {
            beginTransaction();
            StockItem removed = em.find(StockItem.class, id);
            em.remove(removed);
            commitTransaction();
        } else {
            throw new IllegalArgumentException("Product not found");
        }
    }

    /**
     * Method for saving given SoldItem to the database
     **/
    @Override
    public void saveSoldItem(SoldItem item) {
        //SoldItem s = new SoldItem(item.getStockItem(), item.getQuantity());
        em.persist(item);
    }

    /**
     * Method for removing given SoldItem from the database
     **/
    @Override
    public void removeSoldItem(SoldItem item) {
        SoldItem soldItem = em.find(SoldItem.class, item.getId());
        if (soldItem != null) {
            beginTransaction();
            em.remove(soldItem);
            commitTransaction();
        } else {
            throw new IllegalArgumentException("Product not found");
        }
    }

    /**
     * Method for updating given SoldItem in the database
     **/
    @Override
    public void updateSoldItem(SoldItem item) {
        if (item != null) {
            beginTransaction();
            em.merge(item);
            commitTransaction();
        }
    }

    /**
     * Method for saving given SoldPurchase to the database
     **/
    @Override
    public void saveSoldPurchase(SoldPurchase soldPurchase) {
        beginTransaction();
        em.persist(soldPurchase);
        commitTransaction();
    }

    /**
     * Method for updating given SoldPurchase (totalQuantity and totalPrice) in the database
     **/
    @Override
    public void updateSoldPurchase(SoldPurchase soldPurchase, double totalQuantity, double totalPrice) {
        beginTransaction();
        soldPurchase.setTotalPrice(totalPrice);
        soldPurchase.setTotalQuantity(totalQuantity);
        em.merge(soldPurchase);
        commitTransaction();
    }

    /**
     * Method for getting all SoldPurchases from the database
     **/
    @Override
    public List<SoldPurchase> findSoldPurchases() {
        return getSoldPurchases();
    }

    /**
     * Method for getting last ten SoldPurchases from the database
     **/
    @Override
    public List<SoldPurchase> findSoldPurchase_lastTen() {
        // Loading last 10 sales should be done using Hibernate's JPQL queries. - OK
        String call = "Select a FROM SoldPurchase a order by a.id desc";
        return em.createQuery(call, SoldPurchase.class).setMaxResults(10).getResultList();
    }

    /**
     * Method for getting all SoldPurchases between given dates from the database
     **/
    @Override
    public List<SoldPurchase> findSoldPurchase_betweenDates(LocalDate begin, LocalDate end) {
        // Loading sales between the dates should be done using Hibernate's JPQL queries. - OK
        String call = "Select a FROM SoldPurchase a where a.localDate BETWEEN '" + begin + "' and '" + end + "'";
        return em.createQuery(call, SoldPurchase.class).getResultList();
    }

    /**
     * Method for getting all SoldItems (as a list) related to given SoldPurchase from the database
     **/
    @Override
    public List<SoldItem> findSoldPurchaseContent(SoldPurchase soldPurchase) {
        if (soldPurchase != null) {
            long id = soldPurchase.getId();
            String call = "Select a FROM SoldItem a where a.soldPurchase.id = " + id;
            return em.createQuery(call, SoldItem.class).getResultList();
        }
        return null;
    }

    @Override
    public void beginTransaction() {
        em.getTransaction().begin();
    }

    @Override
    public void rollbackTransaction() {
        em.getTransaction().rollback();
    }

    @Override
    public void commitTransaction() {
        em.getTransaction().commit();
    }

   /* public List<StockItem> getStockItems() {
        return em.createQuery("FROM StockItem", StockItem.class).getResultList();
    }*/

    /**
     * Method for getting all SoldItems (as a list) from the database
     **/
    public List<SoldItem> getSoldItems() {
        return em.createQuery("Select a FROM SoldItem a", SoldItem.class).getResultList();
    }

    /**
     * Method for getting all Categories (as a list) from the database
     **/
    public List<Category> getCategories() {
        return em.createQuery("Select a FROM Category a", Category.class).getResultList();
    }

    /**
     * Method for getting all SoldPurchases (as a list) from the database
     **/
    public List<SoldPurchase> getSoldPurchases() {
        return em.createQuery("Select a FROM SoldPurchase a", SoldPurchase.class).getResultList();
    }
}
package ee.ut.math.tvt.salessystem.dao;

import ee.ut.math.tvt.salessystem.dataobjects.Category;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.SoldPurchase;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class InMemorySalesSystemDAO implements SalesSystemDAO {

    private final List<StockItem> stockItemList;
    private final List<SoldItem> soldItemList;
    private final List<Category> categoryList;
    private final List<SoldPurchase> soldPurchaseList;

    public InMemorySalesSystemDAO() {
        // https://edsoehnel.com/retail-cpg-grocery-categories/
        List<StockItem> stockitems = new ArrayList<StockItem>();
        List<Category> categories = new ArrayList<Category>();
        List<SoldItem> solditems = new ArrayList<>();
        categories.add(new Category(1L, "Chips", false));
        categories.add(new Category(2L, "Sweets", false));
        categories.add(new Category(3L, "Sausages", false));
        categories.add(new Category(4L, "Beer/Ale/Alcoholic Cider", true));
        stockitems.add(new StockItem(1L, "Lays chips", "Potato chips", 2.5, 5, 10, categories.get(0)));
        stockitems.add(new StockItem(2L, "Chupa-chups", "Sweets", 0.5, 8, 10, categories.get(1)));
        stockitems.add(new StockItem(3L, "Frankfurters", "Beer sauseges", 5.0, 12, 10, categories.get(2)));
        stockitems.add(new StockItem(4L, "Beer Pohjala", "Student's delight", 3.0, 100, 50, categories.get(3)));
        SoldItem soldOne = new SoldItem(stockitems.get(0), 2);
        SoldItem soldTwo = new SoldItem(stockitems.get(3), 1);
        solditems.add(soldOne);
        solditems.add(soldTwo);
        this.stockItemList = stockitems;
        this.categoryList = categories;
        this.soldItemList = new ArrayList<>();
        this.soldPurchaseList = new ArrayList<>();
        saveSoldItem(soldOne);
        saveSoldItem(soldTwo);
        saveSoldPurchase(new SoldPurchase(solditems, 0));
    }

    @Override
    public List<StockItem> findStockItems() {
        return stockItemList;
    }

    @Override
    public List<SoldItem> findSoldItems() {
        return soldItemList;
    }

    @Override
    public StockItem findStockItem(long id) {
        for (StockItem item : stockItemList) {
            if (item.getId() == id)
                return item;
        }
        return null;
    }

    @Override
    public List<StockItem> findStockItemsLowQuantity() {
        List<StockItem> found = new ArrayList<>();
        for (StockItem item : stockItemList) {
            if (item.getLowQuantity() >= item.getQuantity())
                found.add(item);
        }
        return found;
    }

    @Override
    public void removeStockItem(long id) {
        StockItem find = findStockItem(id);
        stockItemList.remove(find);
    }

    @Override
    public Category findCategory(long id) {
        for (Category cat : categoryList) {
            if (cat.getId() == id)
                return cat;
        }
        return null;
    }

    @Override
    public List<StockItem> findStockItem_name(String name) {
        List<StockItem> found = new ArrayList<>();
        for (StockItem item : stockItemList) {
            if (item.getName().toLowerCase().contains(name.toLowerCase()))
                found.add(item);
        }
        return found;
    }

    @Override
    public List<StockItem> findStockItem_category(String name) {
        List<StockItem> found = new ArrayList<>();
        for (StockItem item : stockItemList) {
            if (item.getCategory().getName().toLowerCase().contains(name.toLowerCase()))
                found.add(item);
        }
        return found;
    }

    @Override
    public void saveSoldItem(SoldItem item) {
        item.setId(soldItemList.size() + 1L);
        soldItemList.add(item);
    }

    @Override
    public void removeSoldItem(SoldItem item) {
        soldItemList.remove(item);
    }

    @Override
    public void updateSoldItem(SoldItem item) {
        soldItemList.remove(item);
        soldItemList.add(item);
    }

    @Override
    public void saveStockItem(StockItem stockItem) {
        beginTransaction();
        if (stockItem.getPrice() < 0 || stockItem.getQuantity() < 0) {
            throw new IllegalArgumentException();
        }
        stockItemList.add(stockItem);
        commitTransaction();
    }

    @Override
    public void updateExistingStockItem(long id, double quantity, double price, boolean increaseQuantity) {
        StockItem item = findStockItem(id);
        if (item != null) {
            if (increaseQuantity) {
                item.increaseQuantity(quantity);
            } else {
                item.decreaseQuantity(quantity);
            }
            item.setPrice(price);
        }
    }

    @Override
    public void saveSoldPurchase(SoldPurchase soldPurchase) {
        soldPurchase.setId(soldPurchaseList.size() + 1L);
        for (SoldItem item : soldPurchase.getItems()) {
            item.setSoldPurchase(soldPurchase);
        }
        soldPurchaseList.add(soldPurchase);
    }

    @Override
    public void updateSoldPurchase(SoldPurchase soldPurchase, double totalQuantity, double totalPrice) {
        soldPurchase.setTotalPrice(totalPrice);
        soldPurchase.setTotalQuantity(totalQuantity);

    }

    @Override
    public List<SoldPurchase> findSoldPurchases() {
        return soldPurchaseList;
    }

    @Override
    public List<SoldItem> findSoldPurchaseContent(SoldPurchase sp) {
        List<SoldItem> soldItems = new ArrayList<>();
        for (SoldItem soldItem : soldItemList) {
            if (soldItem.getSoldPurchase() == sp) {
                soldItems.add(soldItem);
            }
        }
        return soldItems;
    }

    @Override
    public List<SoldPurchase> findSoldPurchase_betweenDates(LocalDate begin, LocalDate end) {
        int from = 0;
        int to = soldPurchaseList.size() - 1;
        while (from < soldPurchaseList.size() && soldPurchaseList.get(from).getLocalDate().compareTo(begin) < 0) {
            from++;
        }
        while (to > -1 && soldPurchaseList.get(to).getLocalDate().compareTo(end) > 0) {
            to--;
        }
        return soldPurchaseList.subList(from, to + 1);
    }

    @Override
    public List<SoldPurchase> findSoldPurchase_lastTen() {
        if (soldPurchaseList.size() <= 10) {
            return soldPurchaseList;
        } else {
            return soldPurchaseList.subList(soldPurchaseList.size() - 10, soldPurchaseList.size());
        }
    }

    public List<Category> getCategories() {
        return categoryList;
    }

    @Override
    public void beginTransaction() {
    }

    @Override
    public void rollbackTransaction() {
    }

    @Override
    public void commitTransaction() {
    }
}

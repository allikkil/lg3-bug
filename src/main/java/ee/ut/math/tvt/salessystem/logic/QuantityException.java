package ee.ut.math.tvt.salessystem.logic;

public class QuantityException extends Exception {

    public QuantityException() {
    }

    public QuantityException(String message) {
        super(message);
    }
}

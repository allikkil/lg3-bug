package ee.ut.math.tvt.salessystem.ui.controllers;

import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.SoldPurchase;
import ee.ut.math.tvt.salessystem.logic.History;
import ee.ut.math.tvt.salessystem.logic.QuantityException;
import ee.ut.math.tvt.salessystem.ui.PopUp;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

/**
 * Encapsulates everything that has to do with the history tab
 * (the tab labelled "History" in the menu).
 * Consists of the history search, sales history table, return section and sold items table.
 **/

public class HistoryController implements Initializable {

    private static final Logger log = LogManager.getLogger(HistoryController.class);
    private final History history;

    @FXML
    private Button showAll;
    @FXML
    private Button showTen;
    @FXML
    private Button showDates;
    @FXML
    private Button showPurchase;
    @FXML
    private Button returnItem;
    @FXML
    private TextField returnQuantity;
    @FXML
    private Text barCode;
    @FXML
    private DatePicker startDate;
    @FXML
    private DatePicker endDate;
    @FXML
    private TableView<SoldPurchase> historyTableView;
    @FXML
    private TableView<SoldItem> selectedPurchaseTableView;
    private SoldPurchase rememberSelectedSoldPurchase;

    public HistoryController(History history) {
        this.history = history;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        refreshHistoryItems();

        selectedPurchaseTableView.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                ObservableList selected = selectedPurchaseTableView.getSelectionModel().getSelectedItems();
                if (selected.size() != 0) {
                    SoldItem selectedItem = (SoldItem) selected.get(0);
                    barCode.setText("Barcode: " + selectedItem.getBarcode().toString());
                    returnItem.setDisable(false);
                }
            }
        });
    }

    /**
     * Event handler for the <code>show all sales history</code> event.
     **/
    @FXML
    public void showAllButtonClicked() {
        refreshHistoryItems();
    }

    /**
     * Event handler for the <code>show last 10 sales history</code> event.
     **/
    @FXML
    public void showLastTenButtonClicked() {
        historyTableView.setItems(FXCollections.observableList(history.getLastTenPurchases()));
        historyTableView.refresh();
    }

    /**
     * Event handler for the <code>show sales history between dates</code> event.
     **/
    @FXML
    public void showBetweenDatesButtonClicked() {
        LocalDate start = startDate.getValue();
        LocalDate end = endDate.getValue();
        try {
            historyTableView.setItems(FXCollections.observableList(history.getSoldPurchasesBetweenDates(start, end)));
            historyTableView.refresh();
        } catch (IllegalArgumentException e) {
            PopUp.displayAlert("Illegal input", e.getMessage());
            log.debug("Illegal quantity input: " + e.getMessage());
        }
    }

    /**
     * Event handler for the <code>return item(s)</code> event.
     **/
    @FXML
    private void returnItemButtonClicked() {
        ObservableList selected = selectedPurchaseTableView.getSelectionModel().getSelectedItems();

        if (selected.size() != 0) {
            SoldItem selectedItem = (SoldItem) selected.get(0);
            try {
                String quantity = returnQuantity.getText();
                history.returnSoldItem(rememberSelectedSoldPurchase, selectedItem, quantity);
                barCode.setText("");
                returnItem.setDisable(true);
                refreshSoldItems(rememberSelectedSoldPurchase);
                log.info("Refreshed SoldItems table");
                refreshHistoryItems();
                log.info("Refreshed SoldPurchases table");
            } catch (QuantityException e) {
                PopUp.displayAlert("Illegal input", e.getMessage());
                log.debug("Illegal quantity input: " + e.getMessage());
            }
            returnQuantity.setText("");
        }
    }

    /**
     * Event handler for the <code>refresh SoldPurchase's SoldItems list</code> event.
     **/
    @FXML
    private void showSoldItemsButtonClicked() {
        ObservableList selected = historyTableView.getSelectionModel().getSelectedItems();
        if (selected.size() != 0) {
            SoldPurchase selectedPurchase = (SoldPurchase) selected.get(0);
            rememberSelectedSoldPurchase = selectedPurchase;
            selectedPurchaseTableView.setItems(FXCollections.observableList(history.getAllSoldItemsOfSelectedSoldPurchase(selectedPurchase)));
            barCode.setText("");
        }
        selectedPurchaseTableView.refresh();
        log.info("Refreshed SoldItems table");
    }

    /**
     * Method for refreshing SoldPurchase table (for reusing purposes).
     **/
    @FXML
    private void refreshHistoryItems() {
        historyTableView.setItems(FXCollections.observableList(history.getAllSoldPurchases()));
        historyTableView.refresh();
        log.info("Refreshed SoldPurchase table");
    }

    /**
     * Method for refreshing SoldItems table (for reusing purposes).
     **/
    private void refreshSoldItems(SoldPurchase soldPurchase) {
        selectedPurchaseTableView.setItems(FXCollections.observableList(history.getAllSoldItemsOfSelectedSoldPurchase(soldPurchase)));
        selectedPurchaseTableView.refresh();
        log.info("Refreshed SoldItems table");
    }

}

package ee.ut.math.tvt.salessystem.ui.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.URL;
import java.nio.file.Paths;
import java.util.Properties;
import java.util.ResourceBundle;

/**
 * Encapsulates everything that has to do with the team tab
 * (the tab labelled "Team" in the menu).
 * Consists of the team members info and a photo.
 **/

public class TeamController implements Initializable {

    private static final Logger log = LogManager.getLogger(TeamController.class);

    @FXML
    private Label teamName = new Label();
    @FXML
    private Label teamLeader = new Label();
    @FXML
    private Label leaderEmail = new Label();
    @FXML
    private Label teamMembers = new Label();
    @FXML
    private ImageView teamImage = new ImageView();

    public TeamController() {
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        // reading the file application.properties from main project resources folder
        try (InputStream input = new FileInputStream(Paths.get("../").toAbsolutePath().normalize().toString() + "\\src\\main\\resources\\application.properties")) {

            Properties prop = new Properties();

            // load the properties file
            prop.load(input);
            log.debug("Loaded the properties file for the team tab");
            // read and initialize the properties needed for this tab
            teamName.setText(prop.getProperty("teamName"));
            teamLeader.setText(prop.getProperty("teamMember1"));
            teamMembers.setText(prop.getProperty("teamMember1") + ", " + prop.getProperty("teamMember2") + ", " + prop.getProperty("teamMember3"));
            leaderEmail.setText(prop.getProperty("leaderEmail"));
            log.debug("Initialized the properties for the team tab");

            // read and initialize the properties needed for the image (image is downloaded from the internet)
            Image image = new Image(prop.getProperty("teamimgURL"));
            teamImage.setImage(image);
            log.debug("Set the image for the team tab");

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}

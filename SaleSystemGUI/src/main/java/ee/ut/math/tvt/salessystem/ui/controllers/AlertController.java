package ee.ut.math.tvt.salessystem.ui.controllers;

import ee.ut.math.tvt.salessystem.ui.PopUp;
import javafx.fxml.Initializable;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Encapsulates everything that has to do with the alerts
 **/

public class AlertController implements Initializable {

    @FXML
    private Label displayMessage;
    @FXML
    private Button okButton;
    @FXML
    private Button cancelButton;

    private String alertMessage;
    private boolean cancelButtonEnabled;

    public AlertController(String alertMessage, boolean buttons) {
        this.alertMessage = alertMessage;
        this.cancelButtonEnabled = buttons;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        displayMessage.setText(alertMessage);
        cancelButton.setVisible(cancelButtonEnabled);
    }

    @FXML
    public void okButtonClicked() {
        Stage stage = (Stage) okButton.getScene().getWindow();
        stage.close();
        PopUp.setButtonResponse(true);
        cancelButton.setVisible(true);
    }

    @FXML
    public void cancelButtonClicked() {
        Stage stage = (Stage) cancelButton.getScene().getWindow();
        stage.close();
        PopUp.setButtonResponse(false);
    }
}

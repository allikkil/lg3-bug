package ee.ut.math.tvt.salessystem.ui;

import ee.ut.math.tvt.salessystem.ui.controllers.AlertController;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.URL;

/**
 * Graphical pop-up alert window
 **/
public class PopUp {

    private static final Logger log = LogManager.getLogger(SalesSystemUI.class);

    private static boolean buttonResponse;

    public static void setButtonResponse(boolean buttonResponse) {
        PopUp.buttonResponse = buttonResponse;
    }

    public static boolean displayAlert(String alertType, String message) {

        boolean buttons = false;
        if (alertType.equals("Stock quantity exceeded")) {
            buttons = true;
        }

        Stage window = new Stage();

        // Block events to other windows
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle(alertType);
        window.setMinWidth(250);

        Group root = new Group();
        Scene scene = new Scene(root, 600, 120, Color.rgb(221, 221, 221));
        String theme = "LightTheme.css";
        scene.getStylesheets().add(SalesSystemUI.class.getResource(theme).toExternalForm());

        BorderPane borderPane = new BorderPane();
        borderPane.prefHeightProperty().bind(scene.heightProperty());
        borderPane.prefWidthProperty().bind(scene.widthProperty());
        try {
            borderPane.setCenter(loadControls(new AlertController(message, buttons)));
        } catch (Exception e) {
            log.debug(e);
        }

        root.getChildren().add(borderPane);

        // Display window and wait for it to be closed before returning
        window.setScene(scene);
        window.showAndWait();

        return buttonResponse;
    }

    private static Node loadControls(Initializable controller) throws IOException {
        URL resource = SalesSystemUI.class.getResource("AlertWindow.fxml");
        if (resource == null) {
            throw new IllegalArgumentException("AlertWindow.fxml" + " not found");
        }

        FXMLLoader fxmlLoader = new FXMLLoader(resource);
        fxmlLoader.setController(controller);
        return fxmlLoader.load();
    }

}

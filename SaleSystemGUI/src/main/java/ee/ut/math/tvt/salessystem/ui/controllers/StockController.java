package ee.ut.math.tvt.salessystem.ui.controllers;

import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.Warehouse;
import ee.ut.math.tvt.salessystem.ui.PopUp;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.net.URL;
import java.util.*;

/**
 * Encapsulates everything that has to do with the warehouse tab
 * (the tab labelled "Warehouse" in the menu).
 * Consists of the add/edit item menu, search and stock items table.
 **/

public class StockController implements Initializable {

    private static final Logger log = LogManager.getLogger(StockController.class);
    private final Warehouse warehouse;

    @FXML
    private Button addItem;
    @FXML
    private Button findSearch;
    @FXML
    private Button cancelSearch;
    @FXML
    private TextField searchField;
    @FXML
    private TableView<StockItem> warehouseTableView;
    @FXML
    private ComboBox comboB;
    @FXML
    private ComboBox changeQuan;
    @FXML
    private TextField idField;
    @FXML
    private TextField nameField;
    @FXML
    private TextField priceField;
    @FXML
    private TextField quantityField;
    @FXML
    private Button clearFields;
    @FXML
    private Button submitItem;
    @FXML
    private Button removeItem;
    @FXML
    private TextField catField;

    public StockController(Warehouse warehouse) {
        this.warehouse = warehouse;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        refreshStockItems();

        this.idField.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if (!newPropertyValue) {
                    fillInputsBySelectedStockItem();
                }
            }
        });
    }

    /**
     * Event handler for the <code>search stock item</code> event.
     **/
    @FXML
    public void searchButtonClicked() {
        List<StockItem> foundStockItems = new ArrayList<>();
        String searchFieldInput = searchField.getText();
        try {
            switch (String.valueOf(comboB.getValue())) {
                case "ID":
                    foundStockItems = warehouse.searchStockItemByBarcode(searchFieldInput);
                    log.debug("Searched warehouse by ID " + searchFieldInput);
                    break;
                case "Name":
                    foundStockItems = warehouse.searchStockItemByName(searchFieldInput);
                    log.debug("Searched warehouse by Name starting with " + searchFieldInput);
                    break;
                case "Category":
                    foundStockItems = warehouse.searchStockItemByCategory(searchFieldInput);
                    log.debug("Searched warehouse by Category " + searchFieldInput);
                    break;
            }
            log.info("Search completed");
        } catch (IllegalArgumentException e) {
            PopUp.displayAlert("Illegal input", e.getMessage());
            log.debug("Illegal search input");
            return;
        }
        warehouseTableView.setItems(FXCollections.observableList(foundStockItems));
        warehouseTableView.refresh();
    }

    /**
     * Event handler for the <code>cancel search</code> event.
     **/
    @FXML
    public void cancelButtonClicked() {
        searchField.setText("");
        warehouseTableView.setItems(FXCollections.observableList(warehouse.getAllStockItems()));
        warehouseTableView.refresh();
        log.debug("Cancel button clicked in the warehouse tab");
    }

    /**
     * Event handler for the <code>refresh warehouse status</code> event.
     **/
    @FXML
    public void refreshButtonClicked() {
        refreshStockItems();
        log.debug("Refresh button clicked in the warehouse tab");
    }

    /**
     * Event handler for the <code>add new item to the warehouse</code> event.
     * Asks for the inputs and gives them to the Warehouse class.
     * Handles all exceptions with alerts to notify the user.
     **/
    @FXML
    private void submitButtonClicked() {
        try {
            long id = Long.parseLong(idField.getText());
            String name = nameField.getText();
            double price = Double.parseDouble(priceField.getText());
            double quantity = Double.parseDouble(quantityField.getText());
            boolean increase;
            if (String.valueOf(changeQuan.getValue()).equals("Increase") || changeQuan.isDisabled()) {
                increase = true;
            } else if (String.valueOf(changeQuan.getValue()).equals("Decrease")) {
                increase = false;
            } else {
                PopUp.displayAlert("Incomplete input",
                        "Please choose if you want to increase or decrease the quantity.");
                log.debug("Illegal quantity input");
                return;
            }
            warehouse.addItem(id, name, quantity, price, increase);
        } catch (IllegalArgumentException e) {
            PopUp.displayAlert("Illegal input(s)",
                    "All fields need to be filled. Price and quantity must be positive.");
            return;
        } catch (IllegalStateException e) {
            PopUp.displayAlert("Illegal input(s)",
                    "You can not decrease the quantity by more than what's in stock");
            return;
        }
        log.info("Item " + nameField.getText() + " was added to the warehouse");
        refreshStockItems();
        clearItem();
    }

    /**
     * Event handler for the <code>remove item from the warehouse</code> event.
     * Asks for the inputs and gives them to the Warehouse class.
     * Handles all exceptions with alerts to notify the user.
     **/
    @FXML
    private void removeItemButtonClicked() {
        try {
            long barcode = Long.parseLong(idField.getText());
            warehouse.removeItem(barcode);
            log.info("Removed stock item with id " + barcode);
            clearItem();
            refreshStockItems();
        } catch (IllegalArgumentException e) {
            PopUp.displayAlert("Illegal input(s)",
                    "The id field must be numeric and positive.");
        } catch (IllegalStateException e) {
            PopUp.displayAlert("Item not found",
                    "Can not remove item, if item is not in stock.");
        }
    }

    /**
     * Method for resetting dialog fields.
     * Event handler for the <code>cancel button clicked</code> event.
     **/
    @FXML
    private void clearItem() {
        idField.clear();
        nameField.clear();
        priceField.clear();
        quantityField.clear();
        catField.clear();
        log.debug("Fields cleared in the warehouse tab");
        changeQuan.setDisable(true);
    }

    /**
     * Method to automatically fill fields when a barcode is inserted.
     **/
    private void fillInputsBySelectedStockItem() {
        long barcode = 0;
        try {
            barcode = Long.parseLong(idField.getText());
            if (barcode < 0) {
                throw new NumberFormatException();
            }
        } catch (Exception e) {
            idField.setText("");
        }
        StockItem stockItem = warehouse.getStockItemByBarcode(barcode);
        if (stockItem != null) {
            changeQuan.setDisable(false);
            nameField.setText(stockItem.getName());
            priceField.setText(String.valueOf(stockItem.getPrice()));
            catField.setText(String.valueOf(stockItem.getCategory()));
            log.debug("Automatically filled the fields in the warehouse tab");
        } else {
            nameField.setText("");
            priceField.setText("");
            quantityField.setText("");
            catField.setText("");
        }
    }

    /**
     * Method for refreshing warehouse status (for reusing purposes).
     **/
    private void refreshStockItems() {
        warehouseTableView.setItems(FXCollections.observableList(warehouse.getAllStockItems()));
        warehouseTableView.refresh();
        log.debug("Stock item list refreshed in the warehouse tab");
    }

}

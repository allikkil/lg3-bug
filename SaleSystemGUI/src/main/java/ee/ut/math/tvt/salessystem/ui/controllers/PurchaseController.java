package ee.ut.math.tvt.salessystem.ui.controllers;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.QuantityException;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import ee.ut.math.tvt.salessystem.ui.PopUp;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Encapsulates everything that has to do with the purchase tab
 * (the tab labelled "Point-of-sale" in the menu).
 * Consists of the purchase menu, current purchase dialog and shopping cart table.
 **/

public class PurchaseController implements Initializable {

    private static final Logger log = LogManager.getLogger(PurchaseController.class);
    private final ShoppingCart shoppingCart;

    @FXML
    private Button newPurchase;
    @FXML
    private Button submitPurchase;
    @FXML
    private Button cancelPurchase;
    @FXML
    private TextField barCodeField;
    @FXML
    private TextField quantityField;
    @FXML
    private TextField nameField;
    @FXML
    private TextField priceField;
    @FXML
    private TextField discountField;
    @FXML
    private Button addItemButton;
    @FXML
    private Button addDiscountButton;
    @FXML
    private Label sumLabel;
    @FXML
    private TableView<SoldItem> purchaseTableView;
    @FXML
    private Button removeButton;
    @FXML
    private ComboBox comboC;

    public PurchaseController(ShoppingCart shoppingCart) {
        this.shoppingCart = shoppingCart;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        cancelPurchase.setDisable(true);
        submitPurchase.setDisable(true);
        purchaseTableView.setItems(FXCollections.observableList(shoppingCart.getAllSoldItemsInShoppingCart()));
        disableProductField(true);


        this.barCodeField.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if (!newPropertyValue) {
                    fillInputsBySelectedStockItem();
                }
            }
        });
    }

    /**
     * Event handler for the <code>new purchase</code> event.
     **/
    @FXML
    protected void newPurchaseButtonClicked() {
        double discount = 0;
        comboC.setValue("Add to");
        log.info("New sale process started");
        try {
            enableInputs();
        } catch (SalesSystemException e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * Event handler for the <code>cancel purchase</code> event.
     **/
    @FXML
    protected void cancelPurchaseButtonClicked() {
        log.info("Sale cancelled");
        try {
            shoppingCart.cancelCurrentPurchase();
            disableInputs();
            sumLabel.setText("0.0");
            purchaseTableView.refresh();
        } catch (SalesSystemException e) {
            log.error(e.getMessage(), e);
        }
    }

    /**
     * Event handler for the <code>submit purchase</code> event.
     **/
    @FXML
    protected void submitPurchaseButtonClicked() {
        try {
            log.debug("Contents of the current basket:\n" + shoppingCart.getAllSoldItemsInShoppingCart());
            shoppingCart.submitCurrentPurchase();
            disableInputs();
            sumLabel.setText("0.0");
            discountField.setText("");
            purchaseTableView.refresh();
        } catch (SalesSystemException e) {
            log.error(e.getMessage(), e);
        }
        log.info("Sale complete");
    }

    /**
     * Event handler for the <code>add discount</code> event.
     **/
    @FXML
    private void addDiscountButtonClicked() {
        try {
            String discountFieldInput = discountField.getText();
            shoppingCart.setDiscount(discountFieldInput);
            sumLabel.setText(shoppingCart.totalSum() + "  (Discount: " + discountFieldInput + "%)");
            log.info("Added discount: " + discountFieldInput + "%");
        } catch (IllegalArgumentException e) {
            PopUp.displayAlert("Illegal input", e.getMessage());
            log.debug("Illegal discount input");
            sumLabel.setText(String.valueOf(shoppingCart.totalSum()));
        }
    }


    /**
     * Add new item to the Shopping Cart.
     **/
    @FXML
    public void addItemEventHandler() {
        String barcode = barCodeField.getText();
        String quantity = quantityField.getText();

        try {
            if (String.valueOf(comboC.getValue()).equals("Add to")) {
                shoppingCart.addSoldItem(barcode, quantity, true);
            } else if (String.valueOf(comboC.getValue()).equals("Remove from")) {
                shoppingCart.removeSoldItem(barcode, quantity);
            } else {
                throw new IllegalArgumentException("Please choose if you want to add to cart or remove from cart");
            }
            resetProductField();
        } catch (Exception e) {
            if (e.getMessage().equals("Stock quantity exceeded")) {
                boolean addMaxQuantityToCart = PopUp.displayAlert("Stock quantity exceeded",
                        "Inserted quantity exceeded stock quantity. The current available quantity is "
                                + shoppingCart.getStockItemQuantity(barcode) + "\n\t\t\t\t\tSell this quantity instead?");
                if (addMaxQuantityToCart) {
                    try {
                        shoppingCart.addSoldItem(barcode, shoppingCart.getStockItemQuantity(barcode), false);
                    } catch (QuantityException r) {
                        log.debug("SoldItem with barcode " + barcode + " has now maximum available StockItem quantity");
                    }
                }
            } else {
                PopUp.displayAlert("Illegal input", e.getMessage());
                return;
            }
        }
        if (shoppingCart.getDiscount() > 0) {
            sumLabel.setText(shoppingCart.totalSum() + "  (Discount: " + shoppingCart.getDiscount() + "%)");
        } else {
            sumLabel.setText(String.valueOf(shoppingCart.totalSum()));
        }
        resetProductField();
        purchaseTableView.refresh();
    }

    /**
     * Remove item from the cart.
     **/
    @FXML
    private void removeItemEventHandler() {
        purchaseTableView.getItems().removeAll(purchaseTableView.getSelectionModel().getSelectedItems());
        if (shoppingCart.getDiscount() > 0) {
            sumLabel.setText(shoppingCart.totalSum() + "  (Discount: " + shoppingCart.getDiscount() + "%)");
        } else {
            sumLabel.setText(String.valueOf(shoppingCart.totalSum()));
        }
        purchaseTableView.refresh();
    }

    /**
     * Sets whether or not the product component is enabled.
     **/
    private void disableProductField(boolean disable) {
        this.addItemButton.setDisable(disable);
        this.barCodeField.setDisable(disable);
        this.quantityField.setDisable(disable);
        this.nameField.setDisable(disable);
        this.nameField.setEditable(false);
        this.priceField.setDisable(disable);
        this.priceField.setEditable(false);
        this.addDiscountButton.setDisable(disable);
        this.comboC.setDisable(disable);
        this.discountField.setDisable(disable);
    }


    // switch UI to the state that allows to proceed with the purchase
    private void enableInputs() {
        resetProductField();
        disableProductField(false);
        cancelPurchase.setDisable(false);
        submitPurchase.setDisable(false);
        newPurchase.setDisable(true);
        log.debug("Purchase input fields and buttons enabled");
    }

    // switch UI to the state that allows to initiate new purchase
    private void disableInputs() {
        resetProductField();
        cancelPurchase.setDisable(true);
        submitPurchase.setDisable(true);
        newPurchase.setDisable(false);
        disableProductField(true);
        log.debug("Purchase input fields and buttons disabled");
    }

    /**
     * Fill dialog inputs automatically if StockItem is found by barcode.
     * Reset product fields in case item with inserted barcode is not found.
     **/
    private void fillInputsBySelectedStockItem() {
        try {
            StockItem stockItem = shoppingCart.getStockItem(barCodeField.getText());
            log.debug("Automatically filled the fields in the purchase tab");
            nameField.setText(stockItem.getName());
            priceField.setText(String.valueOf(stockItem.getPrice()));
        } catch (Exception e) {
            resetProductField();
        }
    }

    /**
     * Reset dialog fields.
     **/
    private void resetProductField() {
        barCodeField.setText("");
        quantityField.setText("1");
        nameField.setText("");
        priceField.setText("");
        log.debug("Reset dialog fields in the purchase tab");
    }
}

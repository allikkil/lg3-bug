# Team LG3-BUG:
1. **Killu Allik**
2. **Karoliine Holter**
3. **Tarmo Pilt**

## Homework 1:
[Link to the homework 1 Wiki](https://bitbucket.org/allikkil/lg3-bug/wiki/Homework%201)

## Homework 2:
[Link to the homework 2 Wiki](https://bitbucket.org/allikkil/lg3-bug/wiki/Homework%202)

## Homework 3:
[Link to the homework 3 Wiki](https://bitbucket.org/allikkil/lg3-bug/wiki/Homework%203)

## Homework 4:
[Link to the homework 4 Wiki](https://bitbucket.org/allikkil/lg3-bug/wiki/Homework%204)

## Homework 5:
[Link to the homework 5 Wiki](https://bitbucket.org/allikkil/lg3-bug/wiki/Homework%205)

## Homework 6:
[Link to the homework 6 Wiki](https://bitbucket.org/allikkil/lg3-bug/wiki/Homework%206)  
[Link to Test plan and Test cases sheet](https://docs.google.com/spreadsheets/d/1lwResv83oBZPgrCjFvNxV2Oyj7BtdRlgzLagctcADJs/edit#gid=0)

## Homework 7:
[Link to the homework 7 Wiki](https://bitbucket.org/allikkil/lg3-bug/wiki/Homework%207)  
[Link to our Usability Test cases](https://docs.google.com/document/d/1OQIGSIAR_MsyDypwowBRUg15jS4IaSz8zhU6lOeqS44/edit?usp=sharing)  
[Link to Test plan and Test cases sheet](https://docs.google.com/spreadsheets/d/1lwResv83oBZPgrCjFvNxV2Oyj7BtdRlgzLagctcADJs/edit#gid=0)  
[Link to Usability Test cases report (our review of LG3-BAB Usability tests)](https://docs.google.com/document/d/1JzD8ys8PscoD5x5qbZF750PZEFuaOkQORpSiJwHfcbs/edit?usp=sharing) 